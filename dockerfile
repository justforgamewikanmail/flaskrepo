FROM python:3.8-slim-buster
# Adding build tools to make yarn install work on Apple silicon / arm64 machines

WORKDIR /app
COPY . .

RUN pip install flask

ENTRYPOINT [ "python" ]
CMD ["app.py"]


